﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {

    private GameManager gameManager;
    
    public float mouseSensitivity = 100;
    public Transform playerBody;

    private float xRotation = 0;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }


    void Update()
    {
        if (gameManager.pageVendeur == 0)
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90, 90);
        
            transform.localRotation = Quaternion.Euler(xRotation,0,0);
            playerBody.Rotate(Vector3.up * mouseX);
        }
    }
}
