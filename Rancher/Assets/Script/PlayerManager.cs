﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{

    private GameManager gameManager;
    
    public CharacterController charac;

    public float speed ;
    public float gravity = -9.81f;
    public float jumpHeight = 2f;
    
    private Vector3 velocity;
    
    public Transform groundCheck;
    public float groundDistance ;
    public LayerMask groundMask;
    
    public bool isGrounded;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
    void Update()
    {
        MovePlayer();
        StopPlayer();
    }

    void MovePlayer()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        if (gameManager.pageVendeur == 0)
        {
            Cursor.lockState = CursorLockMode.Locked;
            
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector3 move = transform.right * x + transform.forward*z;
        
            charac.Move(move * speed * Time.deltaTime);

            if (Input.GetButtonDown("Jump") && isGrounded)
            {
                velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
            }
        
            velocity.y += gravity * Time.deltaTime;
            charac.Move(velocity *  Time.deltaTime);
            speed = 12;
        }
    }

    void StopPlayer()
    {
        if (gameManager.pageVendeur != 0)
        {
            Cursor.lockState = CursorLockMode.None;
            speed = 0;
        }
    }
}
