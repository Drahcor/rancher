﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonManager : MonoBehaviour {

    public GameObject acheterBouton;
    public GameObject vendreBouton;
    public GameObject acheterComcombre;
    public GameObject acheterTomate;
    public GameObject acheterFruit;
    public GameObject returnButton;

    public GameObject instiateHere;

    public GameObject graineComcombre;
    public GameObject graineTomate;

    private GameManager gameManager;

    void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
    }
    
    void Update()
    {
        //mettre les bonnes pages
        // si voit l'ordi, le int page passe a 1 et on voit les deux premier bouton
        if (gameManager.pageVendeur == 1)
        {
            acheterBouton.SetActive(true);
            vendreBouton.SetActive(true);
        }
        else if(gameManager.pageVendeur != 1)
        {
            acheterBouton.SetActive(false);
            vendreBouton.SetActive(false);
        }
        // si l'on appuie sur acheter le int page passe a deux et l'on voit d'autres bouton
        if(gameManager.pageVendeur == 2)
        { 
            acheterComcombre.SetActive(true);
            acheterTomate.SetActive(true);
            acheterFruit.SetActive(true);
            returnButton.SetActive(true);
        }
        else if (gameManager.pageVendeur != 2)
        {
            acheterComcombre.SetActive(false);
            acheterTomate.SetActive(false);
            acheterFruit.SetActive(false);
            returnButton.SetActive(false);
        }
    }
    
    public void VendreObject()
    {
        if (gameManager.pickUp != null) // doit tenir un obj
        {
            if (gameManager.pickUp.CompareTag("fruit"))
            {
                gameManager.money += 10;
                Destroy(gameManager.pickUp);
                gameManager.pageVendeur = 1;
            }
            else if (gameManager.pickUp.CompareTag("comcombre"))
            {
                gameManager.money += 20;
                Destroy(gameManager.pickUp);
                gameManager.pageVendeur = 1;
            }
            else if(gameManager.pickUp.CompareTag("tomate"))
            {
                gameManager.money += 5;
                Destroy(gameManager.pickUp);
                gameManager.pageVendeur = 1;
            }
        }
    }

    public void AcheterObject()
    {
        gameManager.pageVendeur = 2;
    }

    public void ReturnButton()
    {
        gameManager.pageVendeur -= 1;
    }

    public void AcheterFruitButton()
    {
        if (gameManager.money >= 10)
        {
            gameManager.money -= 10;
            var instanciateFruit = Instantiate(gameManager.fruit, transform.position, Quaternion.identity);
            instanciateFruit.transform.position = instiateHere.transform.position;
        }
        else
        {
            print("pas assez argent");
        }
    }
    public void AcheterComcombreButton()
    {
        if (gameManager.money >= 5)
        {
            gameManager.money -= 5;
            var instanciateFruit = Instantiate(graineComcombre, transform.position, Quaternion.identity);
            instanciateFruit.transform.position = instiateHere.transform.position;
        }
        else
        {
            print("pas assez argent");
        }
    }
    public void AcheterTomateButton()
    {
        if (gameManager.money >= 15)
        {
            gameManager.money -= 15;
            
            var instanciateFruit = Instantiate(graineTomate, transform.position, Quaternion.identity);
            instanciateFruit.transform.position = instiateHere.transform.position;
        }
        else
        {
            print("pas assez argent");
        }
    }
}
