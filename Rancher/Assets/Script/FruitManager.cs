﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitManager : MonoBehaviour {

    public PlanteManager planteManager;
    public GameManager gameManager;

    public int stopProduction;
    public float timer;

    void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        
        if (gameObject.name=="fruitComcombre")
        {
            planteManager = transform.parent.GetComponent<PlanteManager>();
        }
        else if (gameObject.name == "bourgeon")
        {
            planteManager = transform.parent.parent.GetComponent<PlanteManager>();
        }
        else if (gameObject.name == "bourgeonTomate")
        {
            planteManager = transform.parent.parent.GetComponent<PlanteManager>();
        }
    }

    void Update()
    {
        if (transform.childCount == 0 && stopProduction < 3) // si n'a pas déjà d'enfant, ou en a deja produit 3
        {
            timer += Time.deltaTime;
            
            if (transform.name == "bourgeon")
            {
                if (timer >= 1200) // vingt minutes
                {
                    var instantiateFruit = Instantiate(gameManager.fruit, transform.position, Quaternion.identity);
                    instantiateFruit.transform.parent = transform;
                    stopProduction += 1;
                }
            }
            if (transform.name == "fruitComcombre")
            {
                if (timer >= 300) // 5 minutes
                {
                    var instanciateComcombre = Instantiate(gameManager.comcombre, transform.position, Quaternion.identity);
                    instanciateComcombre.transform.parent = transform;
                    stopProduction += 1;
                }
            }

            if (transform.parent.parent.CompareTag("piedTomate"))
            {
                if (timer >= 600) // 10 minutes 
                {
                    var instanciateTomate = Instantiate(gameManager.tomate, transform.position, Quaternion.identity);
                    instanciateTomate.transform.parent = transform;
                    instanciateTomate.transform.position = transform.position;
                    stopProduction += 1;
                }
            }
        }
    }
}
