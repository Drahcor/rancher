﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureManager : MonoBehaviour {

    public float life;
    public float lifeMax;
    public Image jaugeVie;
    public Image jaugeDressage;
    
    private Image instantiateJaugeVie;
    private Image instantiateJaugeCapture;

    public bool sauvage;

    public bool attaque;
    public float speed;
    public float timerEnnemy = 60f;
    
    private GameObject player;
    private GameManager gameManager;
    private GameObject canvas;

    private 
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        canvas = GameObject.Find("Canvas");

        sauvage = true;

        instantiateJaugeVie = Instantiate(jaugeVie, Vector3.zero, Quaternion.identity, canvas.transform);
        instantiateJaugeVie.transform.SetParent(canvas.transform);

        instantiateJaugeCapture = Instantiate(jaugeDressage, Vector3.zero, Quaternion.identity, canvas.transform);
        instantiateJaugeCapture.transform.SetParent(canvas.transform);
        
        instantiateJaugeVie.enabled = false;
        instantiateJaugeCapture.enabled = false;
        
    }

    void Update()
    {
        Dressage();

        instantiateJaugeVie.fillAmount = life / lifeMax;
        float distance = Vector3.Distance(player.transform.position, transform.position);
        
        // mettre la jauge de vie au dessus des ennemis
        Vector3 posJauge = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x ,transform.position.y + 1,transform.position.z));
        instantiateJaugeVie.transform.position = posJauge;

        if (life < lifeMax)
        {
            instantiateJaugeVie.enabled = true;
        }
        // ennemis suit le joueur
        if (distance < 12 && life > 0)
        {
            Attack();
            attaque = true;
        }
        else
        {
            attaque = false;
            Attack();
        }
        
        //Dressage si n'a plus de vie
        if (life <= 0)
        {
            sauvage = false;
            transform.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
            transform.LookAt(player.transform);
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("balle"))
        {
            life -= 10;
        }

        if (other.gameObject.CompareTag("Player") && attaque)
        {
            gameManager.playerLife -= 10;
        }
    }
    private void Attack()
    {
        if (attaque && sauvage)
        {
            speed = 3;
            transform.position += transform.forward * (speed * Time.deltaTime);
            transform.LookAt(player.transform);
        }
        else if(!attaque)
        {
            speed = 0;
            transform.position = transform.position;
        }
    }

    private void Dressage()
    {
        if (!sauvage)
        {

            Vector3 posjaugeDressage = Camera.main.WorldToScreenPoint(new Vector3(transform.position.x ,transform.position.y + 1,transform.position.z));
            instantiateJaugeCapture.transform.position = posjaugeDressage;

            instantiateJaugeCapture.enabled = true;
            
            if (timerEnnemy >= 60)
            {
                instantiateJaugeCapture.fillAmount -= 1.0f /timerEnnemy * Time.deltaTime;
            }
        }

        if (instantiateJaugeCapture.fillAmount <= 0)
        {
            print("trop tard");
        }
    }
}
