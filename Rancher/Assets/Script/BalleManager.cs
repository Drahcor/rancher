﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalleManager : MonoBehaviour {
    
    public float shootForce;
    void Start()
    {
    }

    void Update()
    {
        transform.position += Camera.main.transform.forward * (shootForce * Time.deltaTime);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("Player"))
        {
            if (!other.gameObject.CompareTag("arme"))
            {
                Destroy(gameObject);
            }
        }
    }
}
