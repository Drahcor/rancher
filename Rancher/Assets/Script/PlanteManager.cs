﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class PlanteManager : MonoBehaviour {

    public GameManager gameManager;

    public bool arrosée;
    public float timer;
    
    void Start()
    {
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
    }

    void Update()
    {
        if (gameObject == gameManager.planteTrouve)
        {
            arrosée = true;
        }

        if (arrosée)
        {
            timer += Time.deltaTime;

            if (timer >= 120f)
            {
                arrosée = false;
            }
        }
        // plante grandissent
        if (gameObject.CompareTag("plantule") && timer >= 10)
        {
            Instantiate(gameManager.plante,transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
        if (gameObject.CompareTag("plante") && timer >= 5)
        {
            Instantiate(gameManager.arbre,transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
        if (gameObject.CompareTag("petitComcombre") && timer >= 3)
        {
            Instantiate(gameManager.comcombreGrand,transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
        if (gameObject.CompareTag("pousseTomate") && timer >= 3)
        {
            Instantiate(gameManager.piedTomate,transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
        print(timer);
    }
}
