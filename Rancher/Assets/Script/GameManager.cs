﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    
    private Camera camera;

    //attraper OBJ
    public GameObject cible;
    public GameObject pickUp;
    public GameObject playerHand;
    
    //planter graine
    private GameObject planterText;
    public GameObject plantule;
    
    // prefab
    public GameObject plante;
    public GameObject arbre;
    public GameObject fruit;
    public GameObject champ;
    public GameObject comcombrePousse;
    public GameObject comcombreGrand;
    public GameObject comcombre;
    public GameObject pousseTomate;
    public GameObject piedTomate;
    public GameObject tomate;
    public GameObject balle;

    //arrose plante
    public GameObject planteTrouve;
    public GameObject champTrouve;
    
    //vendre / acheter Objets
    public int money;
    public Text moneyText;
    public int pageVendeur;

    //jauge vie
    public float playerLife, playerMaxlife;
    public Image jaugeVie;
    
    public Image Saucisse;
    public float jaugeFaim, jaugeFaimMax;
    public float meurtDeFaim, timerFaim;
    
    void Start()
    { 
        camera= Camera.main;
     playerHand = GameObject.FindWithTag("Player").transform.GetChild(0).gameObject;
     planterText = GameObject.Find("Texteplanter");
     
     playerLife = playerMaxlife;
     jaugeFaim = jaugeFaimMax;
    }
    
    void Update()
    {
        moneyText.text = money.ToString();
        
        SelectionnerOBJ();
        LacherOBJ();
        MangerFruit();
    }

    void SelectionnerOBJ()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out hit, 2.5f))
        {
            // selectionner OBJ
            cible = hit.collider.gameObject;
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("attrapable"))
            {
                // attrape OBJ
                if (Input.GetButtonDown("Fire1") && pickUp == null)
                {
                    pickUp = hit.collider.gameObject;
                    TiensOBJ();
                }
            }
            //Action avec les OBJ
            // planter graine
            if (hit.collider.gameObject.layer == LayerMask.NameToLayer("ChampFertile")) 
            {
                champTrouve = hit.collider.gameObject;
                
                    if (Input.GetKeyDown("e") && champTrouve.transform.childCount <= 0)
                    {
                        if (pickUp != null && pickUp.CompareTag("graineArbre"))
                        {
                            Instantiate(plantule, hit.collider.gameObject.transform.position, Quaternion.identity);
                            Destroy(pickUp);
                            Destroy(champTrouve);
                        }
                        else if (pickUp != null && pickUp.CompareTag("graineComcombre"))
                        {
                            Instantiate(comcombrePousse, hit.collider.gameObject.transform.position, Quaternion.identity);
                            Destroy(pickUp);
                            Destroy(champTrouve);
                        }
                        else if (pickUp != null && pickUp.CompareTag("graineTomate"))
                        {
                            Instantiate(pousseTomate, hit.collider.gameObject.transform.position, Quaternion.identity);
                            Destroy(pickUp);
                            Destroy(champTrouve);
                        }
                    }
                    // faire apparaitre texte planter graine
                    if (pickUp != null && pickUp.CompareTag("graineArbre"))
                    {
                        planterText.SetActive(true);

                    }
                    else if (pickUp != null && pickUp.CompareTag("graineComcombre"))
                    {
                        planterText.SetActive(true);

                    }
                    else if (pickUp != null && pickUp.CompareTag("graineTomate"))
                    {
                        planterText.SetActive(true);
                    }
                    // détruire les champs
                    else if (pickUp != null && pickUp.CompareTag("pelle") && Input.GetButtonDown("Fire1"))
                    {
                        Destroy(champTrouve);
                    }
            }
            else 
            {
                planterText.SetActive(false);
                champTrouve = null;
            }
            // acheter/ vendre graine sur ordinateur
            if (cible.gameObject.layer == LayerMask.NameToLayer("ordinateur"))
            {
                if (Input.GetButtonDown("Fire1") && pageVendeur == 0)
                {
                    pageVendeur = 1;
                }
                if (Input.GetKeyDown("escape"))
                {
                    pageVendeur = 0;
                }
            }
            // arroser plante
            if (pickUp != null && pickUp.CompareTag("arrosoir"))
            {
                if (Input.GetButtonDown("Fire1"))
                {
                    planteTrouve = hit.collider.gameObject;
                }
            }
            // creer nouveau champ
            if (pickUp != null && pickUp.CompareTag("houe"))
            {
                if (Input.GetButtonDown("Fire1") && hit.collider.gameObject.layer == LayerMask.NameToLayer("ground"))
                {
                    Instantiate(champ, new Vector3 (hit.point.x,hit.point.y +0.1f,hit.point.z), Quaternion.identity);
                }
            }
        }
        // si ne voit aucun OBJ
        else
        {
            cible = null;
            planterText.SetActive(false);
            champTrouve = null;
        }
        // mode FPS
        if (pickUp != null && pickUp.CompareTag("arme"))
        {
            if (Input.GetButtonDown("Fire1"))
            {
                Instantiate(balle, ray.origin, Quaternion.identity);
            }
        }
    }

    void TiensOBJ()
    {
        if (playerHand.transform.childCount <= 0)
        {
            pickUp.GetComponent<Rigidbody>().useGravity = false;
            pickUp.GetComponent<Rigidbody>().isKinematic = true;

            pickUp.GetComponent<Collider>().enabled = false;

            pickUp.transform.parent = playerHand.transform;
            pickUp.transform.position = playerHand.transform.position;
            pickUp.transform.rotation = new Quaternion(0,0,0,0);
        }
        else
        {
            pickUp = null;
        }
    }
    void LacherOBJ()
    {
        if (Input.GetKeyDown("e") && playerHand.transform.childCount > 0)
        {
            pickUp.GetComponent<Rigidbody>().useGravity = true;
            pickUp.GetComponent<Rigidbody>().isKinematic = false;
            
            pickUp.GetComponent<Collider>().enabled = true;
                    
            pickUp.transform.parent = null;
            pickUp = null;
        }
    }

    void MangerFruit()
    {
        timerFaim += Time.deltaTime;
        jaugeVie.fillAmount = playerLife / playerMaxlife;
        Saucisse.fillAmount = jaugeFaim / jaugeFaimMax;

        if (Input.GetButtonDown("Fire2"))
        {
            if (pickUp.CompareTag("fruit"))
            {
                Destroy(pickUp);
                jaugeFaim += 1;
            } 
            else if (pickUp.CompareTag("tomate"))
            {
                Destroy(pickUp);
                jaugeFaim += 3;
            } 
            else if (pickUp.CompareTag("comcombre"))
            {
                Destroy(pickUp);
                jaugeFaim += 7;
            }
        }
        // jauge faim diminue
        if (timerFaim >= 10)
        {
            timerFaim = 0;
            jaugeFaim -= 10;
        }
        //affamé
        if (jaugeFaim == 0)
        {
            meurtDeFaim += Time.deltaTime;
            timerFaim = 0;

            if (meurtDeFaim >= 5f)
            {
                meurtDeFaim = 0;
                playerLife -= 10;
            }
        }
        else
        {
            meurtDeFaim = 0;
        }
    }
}
